'use client';
import React, { useState, ChangeEvent, FormEvent } from 'react';

interface GenbankInfo {
  accession: string;
}

const GenbankInfoSection: React.FC = () => {
  const [accession, setAccession] = useState<string>('');
  
  const [genbankInfo, setGenbankInfo] = useState<GenbankInfo | null>(null);

  const handleChange = (e: ChangeEvent<HTMLInputElement>) => {
    setAccession(e.target.value);
  };

  const handleSubmit = async (e: FormEvent<HTMLFormElement>) => {
    e.preventDefault();

    try {
      const response = await fetch('/api/genbank_info', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({ accession }),
      });

      if (response.ok) {
        const data: GenbankInfo = await response.json();
        setGenbankInfo(data);
      } else {
        console.error('Error al llamar al servicio web');
        setGenbankInfo({ accession: ''});
      }
    } catch (error) {
      console.error('Error al llamar al servicio web', error);
      setGenbankInfo({ accession: ''});
    }
  };

  return (
    <div>
      <h2>Consulta de GenBank</h2>
      <form onSubmit={handleSubmit}>
        <label>
          Ingrese el número de acceso del GenBank:
          <input className='bg-white shadow-md rounded px-8 pt-6 pb-8 mb-4'
            type="text"
            value={accession}
            onChange={handleChange}
          />
        </label>
        <button className='bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 mb-4 px-4 rounder' type="submit">Consultar</button>
      </form>
      
      {/* Muestra la información del GenBank */}
      {genbankInfo && (
        <div>
          <h3>Información del GenBank:</h3>
          <pre>{JSON.stringify(genbankInfo, null, 2)}</pre>
        </div>
      )}
    </div>
  );
};

export default GenbankInfoSection;