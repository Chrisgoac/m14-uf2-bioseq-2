from flask import Flask, request, jsonify, Response, json
import sequence
from Bio.Seq import Seq
from Bio.SeqUtils import seq3
from collections import Counter
import time
import biotools
#from flask_cors import CORS

app = Flask(__name__, static_folder="out", static_url_path="/")
#cors = CORS(app, resources={r"/translate": {"origins": "http://localhost:3000"}})

@app.route("/")
def index():
    return app.send_static_file("index.html")

@app.route("/api/time")
def get_current_time():
    return {"time": time.time()}

## Web Services Sessió 2.

@app.route("/api/sequence/<seq>")
def get_sequence_info(seq):
    info = sequence.info(seq)
    return {"info": info}

@app.route('/api/processar_adn', methods=['POST'])
def processar_adn():
    data = request.get_json()

    if 'seq_adn' in data:
        seq_adn = data['seq_adn']
        resultat = biotools.processar_seq_adn(seq_adn)
        return jsonify(resultat)
    else:
        return jsonify({"success": False, "error": "La seqüència d'ADN no s'ha proporcionat."})

## Backend practica 3

@app.route('/api/translate', methods=['POST'])
def translate_sequence():
    try:
        data = request.get_json()
        dna_sequence = data.get('sequence', '').upper() 
        codon_table = int(data.get('codon_table', 1))

        # Test:
        # ATGGTCTAGCTTCCTCCGCTTCTCTCATGCTGCTTCCGGTTCCAGGTGGTGCAAGACAGCCGCTTGGTGGTCTCCGTGCAGTTCCCTGACATGCGCAGGCTGTGGCCGTGGAAGGAGGAGGTGGGAGGAGGCTGTGGGCAGCCTGTGGTCCAGGAGGAGGAGGAGGTGTGTGGTGCGTGTGAGGAGGAGGAGGAGGCAGGAGGAGGTGTGAGGAAGGAGGAGGTGGTGGAAGGAGGTGTGTGGAGGGTGAGGAGGTGTGTGGAGGAGGTGGAAGGAGGAGGAGGTGGTGGAGGAGGTGTGTGGAGGAGGAGGTGGTGGAAGGAGGTGTGTGGAGGAGGTGGAAGGAGGTGTGTGGAGGAGGTGTGAGGAGGTGGAAGGAGGAGGAGGTGGTGGAGGAGGTGTGAGGAGGTGTGAGGAGGTGAGGAGGTGTGTGAGGAGGAGGTGTGAGGAGGTGAGGAGGAGGTGTGTGTGAGGAGGTGTGAGGAGGTGTGTGAGGAGGTGAGGAGGTGTGTGAGGAGGTGTGAGGAGGTGTGAGGAGGTGTGTGAGGAGGTGTGTGTGTGAGGAGGTGTGTGAGGAGGTGTGAGGAGGAGGTGTGAGGAGGTGTGTGAGGAGGTGTGTGAGGAGGTGTGAGGAGGTGTGAGGAGGTGTGTGAGGAGGTGTGTGAGGAGGTGTGTGAGGAGGTGAGGAGGTGTGAGGAGGTGTGTGAGGAGGTGTGAGGAGGTGTGAGGAGGTGTGAGGAGGTGTGTGAGGAGGTGTGTGAGGAGGTGAGGAGGTGTGTGAGGAGGTGTGAGGAGGTGTGAGGAGGTGTGAGGAGGTGTGTGAGGAGGTGTGTGAGGAGGTGTGTGAGGAGGTGAGGAGGTGTGTGAGGAGGTGTGAGGAGGTGTGTGAGGAGGTGTGTGAGGAGGTGAGGAGGTGTGAGGAGGTGTGAGGAGGTGTGTGAGGAGGTGTGAGGAGGTGTGAGGAGGTGTGTGAGGAGGTGTGTGAGGAGGTGTGAGGAGGTGTGAGGAGGTGTGAGGAGGTGTGTGAGGAGGTGTGTGAGGAGGTGTGT

        if not dna_sequence or not all(base in 'ACGT' for base in dna_sequence):
            return jsonify({'error': 'Secuencia de ADN inválida. Solo se permiten las bases A, C, G y T.'}), 400

        translated_sequence = Seq(dna_sequence).translate(table=codon_table)

        cds_segments, incomplete_cds = biotools.analyze_cds(str(translated_sequence))

        rna_sequence = Seq(dna_sequence).transcribe()

        length = len(translated_sequence)
        is_multiple_of_3 = length % 3 == 0

        amino_acid_counts = dict(Counter(translated_sequence))

        result = {
            'translated_sequence': str(translated_sequence),
            'rna_sequence': str(rna_sequence),
            'cds_segments': cds_segments,
            'incomplete_cds': incomplete_cds,
            'length': length,
            'is_multiple_of_3': is_multiple_of_3,
            'amino_acid_counts': amino_acid_counts
        }

        return jsonify(result)

    except Exception as e:
        return jsonify({'error': str(e)}), 500

## Web Services Sessió 4.

@app.route('/api/genbank_info/<accession>', methods=['GET'])
def genbank_info_get(accession):
    if request.method == 'GET':
        info = biotools.get_genbank_info(accession)
        return jsonify(info)
        

@app.route('/api/genbank_info', methods=['POST'])
def genbank_info_post():
    if request.method == 'POST':
        data = request.get_json()
        if 'accession' in data:
            sequence = data['accession']
            info = biotools.get_genbank_info(sequence)
            return jsonify(info)
        else:
            return jsonify({'error': 'No se ha proporcionado la secuencia.'})

@app.route('/fasta/<code>', methods=['GET'])
def get_fasta(code):
    info = biotools.get_fasta_info(code)
    return jsonify(info)

@app.route('/genbank/<accession>', methods=['GET'])
def get_genbank(accession):
    info = biotools.get_genbank_info(accession)
    return jsonify(info)

@app.route('/api/genbank-list', methods=['GET'])
def get_genbank_list():
    genbank_files = biotools.get_genbank_files_list()
    genbank_info_list = []
    # print(genbank_files)
    for accession in genbank_files:
        genbank_info_list.append(biotools.get_genbank_info(accession))

    return jsonify(genbank_info_list)



if __name__ == "__main__":
    app.run(debug=True)

# seq = Seq("AGTA")
# assert Seq("TCAT") == seq.complement()

# def load_seq():
#     # curl https://www.ebi.ac.uk/ena/browser/api/fasta/AP006852.1?download=true -o ap006852.fasta
#     records = list(SeqIO.parse("ap006852.fasta", "fasta"))
#     dna = records[0]

#     print(dna.name)
#     print(dna.description)
#     print(dna.seq[:100])
